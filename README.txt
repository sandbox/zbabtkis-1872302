			/******************/
			 Views Node [Edit]
			 /*****************/

 Author: Zachary Babtkis
 Development Began 2012-12-1
 For Drupal 7

Views Node Edit (VNE) adds an edit column to Views tables, which links to the 
node edit form for the corresponding node. Using the configuration menu, you
can chose which views these edit links should appear on.

**SETUP**

By default, VNE won't enabe the edit links on any of your views. To enable it on
one, several or all views, navigate to admin/config/views-node-edit and chose
the views for which you want to enable VNE. That's it!

**AJAX**
Curently, VNE does not support views that use Ajax. If you enable VNE on a view
that uses Ajax, VNE will have no effect on your view. Ajax compatibility will be
added in a later release.

**Contribute**

To contribute to this project you can clone using the command:

git clone --recursive --branch master http://git.drupal.org/sandbox/zbabtkis/1872302.git views_node_edit



				**********
				*        *
				* Enjoy! *
				*        *
				**********

