(function ($) {
 // Ajax callback for pagination event.
 Drupal.behaviors.views_node_edit = {
  attach: function() {
   var table_names = Drupal.settings.table_names;
   for(table in table_names) {
    var t = table_names[table];
    var results = Drupal.settings[t].results;
    customizeTable(t, results);
   }
  }
 };
 function customizeTable(t,results) {
  // Check to see if view already exists.
  if(!$('.views-node-edit.' + t).length){
   // Check if we are on the correct page.
   if(typeof results != 'undefined') {
    buildTable(t,results);
   }
  }
 }
 function buildTable(table_name, results) {
  var view_node_types = Drupal.settings.view_node_types;
  if(view_node_types instanceof Array) {
    for(type in view_node_types) {
      type = view_node_types[type];
      if(type.field == 'node.type') {
        var types_query_string = '/vne/add/' + type['value'].join(',');
      }
    }
  } else {
    types_query_string = '/node/add';
  }
  $('.' + table_name + ' thead tr').prepend(
  	  '<th class=\"views-node-edit views-node-edit-head ' + table_name + 
  	  '\"><a class="add-node-of-type" href="' + types_query_string + '">+</a></th>');
  for(var row_counter = 0; row_counter < results.length; row_counter++) {
   // Gets the nid of the node that will be added to the edit link.
   var nid = results[row_counter].nid;
   // Adds edit field to row.
   $('.' + table_name + ' tbody tr:eq(' + row_counter + ')').prepend(
    '<td class=\"views-node-edit ' + table_name + '\"><a href=\"/node/' + nid + 
    '/edit\">[edit]</a></td>');
  }
 }
})(jQuery);

